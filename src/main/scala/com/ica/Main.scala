package com.ica

import com.ica.commons.Properties
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{Row, SparkSession}
import java.io.FileNotFoundException
import java.io.IOException


/**
 * Main is the entry point of the application
 * This function invokes the respective functions
 */
object Main {

  /**
   * This function accepts the arguments and creates the dataframe
   * and clenses the records which doesn't have the reading recorded.
   */
  def main(args: Array[String]): Unit = {
    // Set the log level to only print errors
    Logger.getLogger("org").setLevel(Level.ERROR)

    try {
      // Use new SparkSession interface in Spark 2.0
      val spark = SparkSession
        .builder
        .appName("environmentAnalysis")
        .master("local[*]")
        .getOrCreate()

      //creating the pressure dataframe from the input path.
      val pressureDf = spark.read.format("csv")
        .option("delimiter", " ")
        .option("header", "true")
        .schema(Properties.pressureSchema)
        .option("inferSchema", "true")
        .load("resources/presssure")

      //creating the temperature dataframe from the input path.
      val temperatureDf = spark.read.format("csv")
        .option("delimiter", " ")
        .option("header", "true")
        .schema(Properties.temperatureSchema)
        .option("inferSchema", "true")
        .load("resources/temperature")

      //clensing the data - droping the rows where the readings are not recorded.
      val finalPressureDf = pressureDf.na.drop()
      val finalTempDf = temperatureDf.na.drop()

      //writing the clensed data back to the disk
      finalPressureDf.write.parquet("resources/output/pressure")
      finalTempDf.write.parquet("resources/output/temperature")

    } catch {
      case ex: FileNotFoundException => {
        logger.error("File not found")
      }
      case ex: IOException => {
        logger.error("IO Exception")
      }
      finally
      {
        logger.error("Unable to create clensed data.Check the schema")
      }
    }
  }
}
